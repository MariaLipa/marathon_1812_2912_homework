# marathon_1812_2912_homework

---

Марафон тривав два тижні і ми зустрічались 6 раз, тобто ви маєте 6 домашніх завдань. Фактично - це і є те саме шосте завдання.
Тепер Вам необхідно впорядкувати всі завдання для перевірки.

Для цього:
- зробіть форк вій цього репозиторію
- налаштуйте в своєму репозиторії все (мова про лінтери, особливості .gitignore - якщо вони для Вас є) як описано в завданні 2
- оновіть головну гілку
- додайте до проекту всіх викладачів (контакти буде в нашому чаті)
- заповніть README.md - там повинен бути опис роботи, хто виконував, особливості роботи якщо вони присутні і Ви вважаєте що той хто працює з Вашим репозиторієм повинен їх знати.
- додайте до проекту 
- виконайте домашні завдання (окрім другого, його виконувати не треба) - КОЖНЕ В ОКРЕМІЙ ГІЛЬЦІ яка має назву "homework_1", "homework_3", ..., відповідно.
- файл(и) з виконаним ДЗ повинен знаходитись у відповіній папці ("day_1", "day_3", ... - поряд з *.md файлом з текстом завдання)

Посилання саме на цей репозиторій внесіть у форму разом з іншими даними для оцінювання, посилання на яку зʼявится в нашому чаті марафону після закінчення останньої зустічі.
